def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return f

def read_all_mat(infile):
    from scipy.io import loadmat
    import h5py
    try:
        d = loadmat(infile)
    except NotImplementedError:
        f = h5py.File(infile)
        d = dict(f)
    return d

